#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_count = 1;
	if (unitPrice > 0) this->_unitPrice = unitPrice;
	else throw std::invalid_argument("unit price is always bigger than zero");
}

Item::Item(const Item& other)
{
	this->_name = other.getName();
	this->_serialNumber = other.getSerialNumber();
	this->_count = other.getCount();
	this->_unitPrice = other.getUnitPrice();
}

Item::~Item() {}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator <(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator >(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator ==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}

void Item::setCount(int count)
{
	this->_count = count;
}

void Item::incCount()
{
	this->_count++;
}

void Item::decCount()
{
	this->_count--;
}

int Item::getCount() const
{
	return this->_count;
}

void Item::setName(std::string name)
{
	this->_name = name;
}

std::string Item::getName() const
{
	return this->_name;
}

void Item::setSerialNumber(std::string serialNumber)
{
	this->_serialNumber = serialNumber;
}

std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

void Item::setUnitPrice(int unitPrice)
{
	this->_unitPrice = unitPrice;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

std::ostream& operator<<(std::ostream& output, const Item& item)
{
	output << item._name << " : " << "price: " << item._unitPrice << std::endl;
	return output;
}
