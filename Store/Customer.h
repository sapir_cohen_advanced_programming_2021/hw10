#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string);
	Customer(const Customer& other);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions

	std::set<Item> getItems();

private:
	std::string _name;
	std::set<Item> _items;

	void changeItem(std::set<Item>::iterator it, Item newItem);
};
