#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string, std::string, double);
	Item(const Item& other);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	friend std::ostream& operator<<(std::ostream& output, const Item& item);

	//get and set functions

	void setCount(int count);
	void incCount();
	void decCount();
	int getCount() const;

	void setName(std::string name);
	std::string getName() const;

	void setSerialNumber(std::string serialNumber);
	std::string getSerialNumber() const;

	void setUnitPrice(int unitPrice);
	double getUnitPrice() const;

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};
