#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer(const Customer& other)
{
	this->_name = other._name;
	this->_items = other._items;
}

Customer::~Customer() {}

double Customer::totalSum() const
{
	double sum = 0;
	for (std::set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	std::set<Item>::iterator it = this->_items.find(item);
	if (it != this->_items.end())
	{
		Item itemChange(*it);
		itemChange.incCount();
		this->changeItem(it, itemChange);
	}
	else this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator it = this->_items.find(item);
	if (it != this->_items.end() && it->getCount() > 1)
	{
		Item itemChange(*it);
		itemChange.decCount();
		this->changeItem(it, itemChange);
	}
	else this->_items.erase(item);
}

void Customer::changeItem(std::set<Item>::iterator it, Item newItem)
{
	this->_items.erase(*it);
	this->_items.insert(newItem);
}

std::set<Item> Customer::getItems()
{
	return this->_items;
}
