#include <vector>
#include"Customer.h"
#include<map>

#define SIZE 10

enum mainChoices { SIGN = 1, UPDATE, PAYS_MOST, EXIT };
enum updateChoice {ADD = 1, REMOVE, BACK};

int getChoice();

void printItems(std::vector<Item> itemV);

void addItems(std::map<std::string, Customer> &customers, Item (&itemList)[SIZE], std::string name);

std::vector<Item> setToVector(std::set<Item> items);

void removeItems(std::map<std::string, Customer> &customers, Item(&itemList)[SIZE], std::string name);

void signUser(std::map<std::string, Customer> &customers , Item(&itemList)[SIZE]);

void printUpdateMenu();

void update(std::map<std::string, Customer> &customers, Item(&itemList)[SIZE]);

std::string getPaidMost(std::map<std::string, Customer> &customers);

void store(std::map<std::string, Customer> customers, Item(&itemList)[SIZE]);


int main()
{
	std::map<std::string, Customer> abcCustomers;
	Item itemList[SIZE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};
	store(abcCustomers, itemList);
	return 0;
}

int getChoice()
{
	int input = 0;
	while (input != SIGN && input != UPDATE && input != PAYS_MOST && input != EXIT)
	{
		std::cout << "Welcome to MagshiMart!" << std::endl;
		std::cout << SIGN << ". to sign as customer and buy items" << std::endl;
		std::cout << UPDATE << ". to uptade existing customer's items" << std::endl;
		std::cout << PAYS_MOST << ". to print the customer who pays the most" << std::endl;
		std::cout << EXIT << ". to exit" << std::endl;
		std::cin >> input;
	}
	return input;
}

void addItems(std::map<std::string, Customer> &customers, Item(&itemList)[SIZE], std::string name)
{
	int buy = 1;
	std::vector<Item> v(itemList, itemList + (sizeof(itemList) / sizeof(itemList[0])));
	while (buy)
	{
		std::cout << "The items you can buy are: (0 to exit)" << std::endl;
		printItems(v);
		std::cout << "What item would you buy? input: ";
		std::cin >> buy;
		std::cout << std::endl;
		if (buy > 0 && buy <= SIZE) customers.find(name)->second.addItem(itemList[buy - 1]);
	}
}

std::vector<Item> setToVector(std::set<Item> items)
{
	std::vector<Item> v;
	for (std::set<Item>::iterator it = items.begin(); it != items.end(); ++it)
	{
		std::set<Item>::iterator itTemp = it;
		v.push_back(*itTemp);
	}
	return v;
}

void removeItems(std::map<std::string, Customer> &customers, Item(&itemList)[SIZE], std::string name)
{
	unsigned int remove = 1;
	Customer customer(customers.find(name)->second);
	std::vector<Item> v = setToVector(customer.getItems());
	while (remove)
	{
		std::cout << "The items you can remove are: (0 to exit)" << std::endl;
		v = setToVector(customer.getItems());
		printItems(v);
		std::cout << "What item would you like to remove? input: ";
		std::cin >> remove;
		std::cout << std::endl;
		if (remove > 0 && remove <= v.size()) customer.removeItem(v[remove - 1]);
	}
	customers.find(name)->second = customer;
}

void signUser(std::map<std::string, Customer> &customers, Item(&itemList)[SIZE])
{
	std::string name;
	std::cin >> name;
	if (customers.find(name) != customers.end()) throw std::invalid_argument("customer already exists!");
	Customer newCustomer(name);
	std::pair<std::string, Customer> add(name, newCustomer);
	customers.insert(add);
	addItems(customers, itemList, name);
}

void printItems(std::vector<Item> itemV)
{
	for (unsigned int i = 0; i < itemV.size(); i++)
	{
		std::cout << i + 1 << ". " << itemV[i];
	}
}

void update(std::map<std::string, Customer> &customers, Item(&itemList)[SIZE])
{
	int choice = 0;
	std::string name;
	std::cin >> name;
	if (customers.find(name) == customers.end()) throw std::invalid_argument("customer does not exist!");

	while (choice != BACK)
	{
		printUpdateMenu();
		std::cin >> choice;
		switch (choice)
		{
		case ADD:
			addItems(customers, itemList, name);
			break;
		case REMOVE:
			removeItems(customers, itemList, name);
			break;
		case BACK:
			break;
		default:
			std::cout << "invalid choice enter again: " << std::endl;
			break;
		}
	}
}

void printUpdateMenu()
{
	std::cout << ADD << ". Add items" << std::endl;
	std::cout << REMOVE << ". Remove items" << std::endl;
	std::cout << BACK << ". Back to menu" << std::endl;
}

std::string getPaidMost(std::map<std::string, Customer> &customers)
{
	std::map<std::string, Customer>::iterator it = customers.begin();
	std::map<std::string, Customer>::iterator itMax = customers.begin();
	if (it != customers.end())
	{
		for (it = it++; it != customers.end(); ++it)
		{
			if (it->second.totalSum() > itMax->second.totalSum()) itMax = it;
		}
	}
	return itMax->first;
}

void store(std::map<std::string, Customer> customers, Item(&itemList)[SIZE])
{
	int choice = 0;
	while ((choice = getChoice()) != EXIT)
	{
		system("CLS");
		switch (choice)
		{
		case SIGN:
			std::cout << "enter name: " << std ::endl;
			try
			{
				signUser(customers, itemList);
			}
			catch (std::invalid_argument& e)
			{
				std::cout << "EXCEPTION: " << e.what() << std::endl;
			}
			system("PAUSE");
			break;
		case UPDATE:
			std::cout << "enter name: " << std::endl;
			try
			{
				update(customers, itemList);
			}
			catch (std::invalid_argument& e)
			{
				std::cout << "EXCEPTION: " << e.what() << std::endl;
			}
			system("PAUSE");
			break;
		case PAYS_MOST:
			std::cout << "The name of the customer who paid the most is : " << getPaidMost(customers) << std::endl;
			system("PAUSE");
			break;
		case EXIT:
			break;
		default:
			std::cout << "invalid choice enter again: " << std::endl;
			break;
		}
		system("CLS");
	}
}
